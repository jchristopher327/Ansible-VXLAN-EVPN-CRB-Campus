version 20191212.201431_builder.r1074901;
system {
    host-name pod1-leaf7;
    root-authentication {
        encrypted-password "$6$eG4FXiQW$J4/a.5GZlXlrKcvJTpbaMl.qIelPqjR5lFk.POmxo5O3Vp5OdjHFFJ97PMqvABuZZOGXrdvSE6YF3f33wilgO.";
    }
    login {
        user netpalm {
            uid 3001;
            class super-user;
            authentication {
                encrypted-password "$6$UNZtRTU2$tf7oY8VIKqRQ53B/qbZzPr7pFMuNOJdbiikmJTNfKaLSnjMuf8a5vDezVKwyckoMyf7JQ4QdOT6Ex1LnUhgzJ/";
            }
        }
        user packetferret {
            uid 3002;
            class super-user;
            authentication {
                encrypted-password "$6$.HKuZDyY$H2rjpnC8wAnzz71hn0WisOjjrg5Ap.Mpqui4/0CMj6G.b5VF6m4ZrsUMkG7stBc3Q8i.HPFleceoM6Kwa7D5c.";
            }
        }
        user cremsburg {
            uid 3003;
            class super-user;
            authentication {
                encrypted-password "$6$JX8v3CWG$oLWOKFK0phWqtGrFx7HdeeVcgFRyQULVxgoTD4jMMGVNA87UUxTOYgewuWRwoosfwFqRTLO8zHQXkGWM0yxmT1";
            }
        }
    }
    time-zone America/Chicago;
    name-server {
        10.6.6.20;
        10.6.6.21;
    }
    services {
        ssh {
            root-login allow;
        }
        netconf {
            ssh;
        }
        rest {
            http {
                port 8080;
            }
            enable-explorer;
        }
    }
    syslog {
        user * {
            any emergency;
        }
        host 10.6.6.101 {
            any any;
            port 5514;
            source-address 192.168.43.209;
            structured-data;
        }
        file messages {
            any notice;
            authorization info;
        }
        file interactive-commands {
            interactive-commands any;
        }
        file default-log-messages {
            any info;
            match "(requested 'commit' operation)|(requested 'commit synchronize' operation)|(copying configuration to juniper.save)|(commit complete)|ifAdminStatus|(FRU power)|(FRU removal)|(FRU insertion)|(link UP)|transitioned|Transferred|transfer-file|(license add)|(license delete)|(package -X update)|(package -X delete)|(FRU Online)|(FRU Offline)|(plugged in)|(unplugged)|GRES";
            structured-data;
        }
    }
}
chassis {
    aggregated-devices {
        ethernet {
            device-count 12;
        }
    }
}
interfaces {
    xe-0/0/6 {
        description "[xe-0/0/6] to pod1-spine1";
        unit 0 {
            family inet {
                address 10.255.255.25/31;
            }
        }
    }
    xe-0/0/7 {
        description "[xe-0/0/7] to pod1-spine2";
        unit 0 {
            family inet {
                address 10.255.255.27/31;
            }
        }
    }
    xe-0/0/9 {
        description "[xe-0/0/9] to server3";
        unit 0 {
            family ethernet-switching {
                interface-mode access;
                vlan {
                    members vlan_103;
                }
            }
        }
    }
    xe-0/0/10 {
        description "[xe-0/0/10] to server2";
        unit 0 {
            family ethernet-switching {
                interface-mode access;
                vlan {
                    members vlan_102;
                }
            }
        }
    }
    xe-0/0/11 {
        description "[xe-0/0/11] to server1";
        unit 0 {
            family ethernet-switching {
                interface-mode access;
                vlan {
                    members vlan_101;
                }
            }
        }
    }
    em0 {
        unit 0 {
            description "out of band interface";
            family inet {
                address 192.168.43.209/24;
            }
        }
    }
    em1 {
        unit 0 {
            family inet {
                address 169.254.0.2/24;
            }
        }
    }
    lo0 {
        unit 0 {
            description "loopback";
            family inet {
                address 192.168.0.17/32;
            }
        }
    }
}
snmp {
    interface em0.0;
    community "$home_snmp$" {
        authorization read-only;
        clients {
            10.6.6.0/24;
            0.0.0.0/0 restrict;
        }
    }
    trap-options {
        source-address 192.168.43.209;
    }
    trap-group space {
        version v2;
        targets {
            10.6.6.120;
        }
    }
}
policy-options {
    policy-statement IMPORT_GLOBAL {
        term GLOBAL {
            from {
                community COMMUNITY_GLOBAL;
            }
            then accept;
        }
        term other {
            then reject;
        }
    }
    community COMMUNITY_GLOBAL members target:101:1111;
}
routing-options {
    static {
        route 10.255.0.0/17 {
            next-hop 192.168.43.1;
            no-readvertise;
        }
        route 10.6.5.0/24 {
            next-hop 192.168.43.1;
            no-readvertise;
        }
        route 10.6.6.0/24 {
            next-hop 192.168.43.1;
            no-readvertise;
        }
        route 10.9.0.0/16 {
            next-hop 192.168.43.1;
            no-readvertise;
        }
    }
    router-id 192.168.0.17;
    autonomous-system 101.1;
}
protocols {
    bgp {
        group EVPN_FABRIC {
            type internal;
            local-address 192.168.0.17;
            family evpn {
                signaling;
            }
            multipath;
            neighbor 192.168.0.1 {
                description "pod1-spine1";
            }
            neighbor 192.168.0.2 {
                description "pod1-spine2";
            }
        }
    }
    evpn {
        encapsulation vxlan;
        extended-vni-list 5101;
        extended-vni-list 5102;
        extended-vni-list 5103;
    }
    lldp {
        port-id-subtype interface-name;
        port-description-type interface-alias;
        interface all;
        interface em0 {
            disable;
        }
    }
    ospf {
        area 0.0.0.0 {
            interface xe-0/0/6.0;
            interface xe-0/0/7.0;
            interface lo0.0 {
                passive;
            }
        }
    }
}
switch-options {
    vtep-source-interface lo0.0;
    route-distinguisher 192.168.0.17:1;
    vrf-import IMPORT_GLOBAL;
    vrf-target {
        target:101:1111;
        auto;
    }
}
vlans {
    vlan_101 {
        description "MANAGEMENT VLAN";
        vlan-id 101;
        vxlan {
            ingress-node-replication;
            vni 5101;
        }
    }
    vlan_102 {
        description "PC VLAN";
        vlan-id 102;
        vxlan {
            ingress-node-replication;
            vni 5102;
        }
    }
    vlan_103 {
        description "WIRELESS VLAN";
        vlan-id 103;
        vxlan {
            ingress-node-replication;
            vni 5103;
        }
    }
}
