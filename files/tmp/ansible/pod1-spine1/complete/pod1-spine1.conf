version 20191212.201431_builder.r1074901;
system {
    host-name pod1-spine1;
    root-authentication {
        encrypted-password "$6$eG4FXiQW$J4/a.5GZlXlrKcvJTpbaMl.qIelPqjR5lFk.POmxo5O3Vp5OdjHFFJ97PMqvABuZZOGXrdvSE6YF3f33wilgO.";
    }
    login {
        user netpalm {
            uid 3001;
            class super-user;
            authentication {
                encrypted-password "$6$UNZtRTU2$tf7oY8VIKqRQ53B/qbZzPr7pFMuNOJdbiikmJTNfKaLSnjMuf8a5vDezVKwyckoMyf7JQ4QdOT6Ex1LnUhgzJ/";
            }
        }
        user packetferret {
            uid 3002;
            class super-user;
            authentication {
                encrypted-password "$6$.HKuZDyY$H2rjpnC8wAnzz71hn0WisOjjrg5Ap.Mpqui4/0CMj6G.b5VF6m4ZrsUMkG7stBc3Q8i.HPFleceoM6Kwa7D5c.";
            }
        }
        user cremsburg {
            uid 3003;
            class super-user;
            authentication {
                encrypted-password "$6$JX8v3CWG$oLWOKFK0phWqtGrFx7HdeeVcgFRyQULVxgoTD4jMMGVNA87UUxTOYgewuWRwoosfwFqRTLO8zHQXkGWM0yxmT1";
            }
        }
    }
    time-zone America/Chicago;
    name-server {
        10.6.6.20;
        10.6.6.21;
    }
    services {
        ssh {
            root-login allow;
        }
        netconf {
            ssh;
        }
        rest {
            http {
                port 8080;
            }
            enable-explorer;
        }
    }
    syslog {
        user * {
            any emergency;
        }
        host 10.6.6.101 {
            any any;
            port 5514;
            source-address 192.168.43.201;
            structured-data;
        }
        file messages {
            any notice;
            authorization info;
        }
        file interactive-commands {
            interactive-commands any;
        }
        file default-log-messages {
            any info;
            match "(requested 'commit' operation)|(requested 'commit synchronize' operation)|(copying configuration to juniper.save)|(commit complete)|ifAdminStatus|(FRU power)|(FRU removal)|(FRU insertion)|(link UP)|transitioned|Transferred|transfer-file|(license add)|(license delete)|(package -X update)|(package -X delete)|(FRU Online)|(FRU Offline)|(plugged in)|(unplugged)|GRES";
            structured-data;
        }
    }
}
chassis {
    aggregated-devices {
        ethernet {
            device-count 12;
        }
    }
}
interfaces {
    xe-0/0/0 {
        description "[xe-0/0/0] to pod1-leaf1";
        unit 0 {
            family inet {
                address 10.255.255.0/31;
            }
        }
    }
    xe-0/0/1 {
        description "[xe-0/0/1] to pod1-leaf2";
        unit 0 {
            family inet {
                address 10.255.255.4/31;
            }
        }
    }
    xe-0/0/2 {
        description "[xe-0/0/0] to pod1-leaf3";
        unit 0 {
            family inet {
                address 10.255.255.8/31;
            }
        }
    }
    xe-0/0/3 {
        description "[xe-0/0/1] to pod1-leaf4";
        unit 0 {
            family inet {
                address 10.255.255.12/31;
            }
        }
    }
    xe-0/0/4 {
        description "[xe-0/0/4] to pod1-leaf5";
        unit 0 {
            family inet {
                address 10.255.255.16/31;
            }
        }
    }
    xe-0/0/5 {
        description "[xe-0/0/5] to pod1-leaf6";
        unit 0 {
            family inet {
                address 10.255.255.20/31;
            }
        }
    }
    xe-0/0/6 {
        description "[xe-0/0/6] to pod1-leaf7";
        unit 0 {
            family inet {
                address 10.255.255.24/31;
            }
        }
    }
    xe-0/0/7 {
        description "[xe-0/0/7] to pod1-leaf8";
        unit 0 {
            family inet {
                address 10.255.255.28/31;
            }
        }
    }
    irb {
        unit 101 {
            family inet {
                address 10.10.101.2/24 {
                    virtual-gateway-address 10.10.101.1;
                }
            }
            family inet6 {
                address 2001:db8::10:10:101:2/112 {
                    virtual-gateway-address 2001:db8::10:10:101:1;
                }
                address fe80::10:10:101:1/112;
            }
        }
        unit 102 {
            family inet {
                address 10.10.102.2/24 {
                    virtual-gateway-address 10.10.102.1;
                }
            }
            family inet6 {
                address 2001:db8::10:10:102:2/112 {
                    virtual-gateway-address 2001:db8::10:10:102:1;
                }
                address fe80::10:10:102:1/112;
            }
        }
        unit 103 {
            family inet {
                address 10.10.103.2/24 {
                    virtual-gateway-address 10.10.103.1;
                }
            }
            family inet6 {
                address 2001:db8::10:10:103:2/112 {
                    virtual-gateway-address 2001:db8::10:10:103:1;
                }
                address fe80::10:10:103:1/112;
            }
        }
    }
    em0 {
        unit 0 {
            description "out of band interface";
            family inet {
                address 192.168.43.201/24;
            }
        }
    }
    em1 {
        unit 0 {
            family inet {
                address 169.254.0.2/24;
            }
        }
    }
    lo0 {
        unit 0 {
            description "loopback";
            family inet {
                address 192.168.0.1/32;
            }
        }
        unit 1 {
            description "loopback";
            family inet {
                address 192.168.1.1/32;
            }
        }
    }
}
snmp {
    interface em0.0;
    community "$home_snmp$" {
        authorization read-only;
        clients {
            10.6.6.0/24;
            0.0.0.0/0 restrict;
        }
    }
    trap-options {
        source-address 192.168.43.201;
    }
    trap-group space {
        version v2;
        targets {
            10.6.6.120;
        }
    }
}
policy-options {
    policy-statement ecmp_policy {
        term 10 {
            then accept;
        }
    }
}
routing-options {
    static {
        route 10.255.0.0/17 {
            next-hop 192.168.43.1;
            no-readvertise;
        }
        route 10.6.5.0/24 {
            next-hop 192.168.43.1;
            no-readvertise;
        }
        route 10.6.6.0/24 {
            next-hop 192.168.43.1;
            no-readvertise;
        }
        route 10.9.0.0/16 {
            next-hop 192.168.43.1;
            no-readvertise;
        }
    }
    router-id 192.168.0.1;
    autonomous-system 101.1;
    forwarding-table {
        export ecmp_policy;
        ecmp-fast-reroute;
    }
}
protocols {
    bgp {
        group EVPN_FABRIC {
            type internal;
            local-address 192.168.0.1;
            family evpn {
                signaling;
            }
            multipath;
            cluster 0.0.0.1;
            neighbor 192.168.0.11 {
                description "pod1-leaf1";
            }
            neighbor 192.168.0.12 {
                description "pod1-leaf2";
            }
            neighbor 192.168.0.13 {
                description "pod1-leaf3";
            }
            neighbor 192.168.0.14 {
                description "pod1-leaf4";
            }
            neighbor 192.168.0.15 {
                description "pod1-leaf5";
            }
            neighbor 192.168.0.16 {
                description "pod1-leaf6";
            }
            neighbor 192.168.0.17 {
                description "pod1-leaf7";
            }
            neighbor 192.168.0.18 {
                description "pod1-leaf8";
            }
            vpn-apply-export;
        }
    }
    lldp {
        port-id-subtype interface-name;
        port-description-type interface-alias;
        interface all;
        interface em0 {
            disable;
        }
    }
    ospf {
        area 0.0.0.0 {
            interface xe-0/0/0.0;
            interface xe-0/0/1.0;
            interface xe-0/0/2.0;
            interface xe-0/0/3.0;
            interface xe-0/0/4.0;
            interface xe-0/0/5.0;
            interface xe-0/0/6.0;
            interface xe-0/0/7.0;
            interface lo0.0 {
                passive;
            }
        }
    }
}
routing-instances {
    VIRTUAL_SWITCH_1 {
        description "VRF for virtual-switch";
        protocols {
            evpn {
                encapsulation vxlan;
                extended-vni-list all;
                default-gateway no-gateway-community;
            }
        }
        vlans {
            vlan_101 {
                vlan-id 101;
                l3-interface irb.101;
                vxlan {
                    vni 5101;
                    ingress-node-replication;
                }
            }
            vlan_102 {
                vlan-id 102;
                l3-interface irb.102;
                vxlan {
                    vni 5102;
                    ingress-node-replication;
                }
            }
            vlan_103 {
                vlan-id 103;
                l3-interface irb.103;
                vxlan {
                    vni 5103;
                    ingress-node-replication;
                }
            }
        }
        vtep-source-interface lo0.0;
        instance-type virtual-switch;
        route-distinguisher 192.168.0.1:1;
        vrf-target {
            target:101:1111;
            auto;
        }
    }
    EVPN_VRF_1 {
        instance-type vrf;
        interface lo0.1;
        interface irb.101;
        interface irb.102;
        interface irb.103;
        route-distinguisher 192.168.1.1:1;
        vrf-target target:64500:1;
        routing-options {
            router-id 192.168.1.1;
            autonomous-system 64500;
        }
    }
}
